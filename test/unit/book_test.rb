require 'test_helper'

class BookTest < ActiveSupport::TestCase
  let(:book) {Book.create!(fullpath: '/Volumes/text/Books/Fiction and Short Stories/1001 Books You Must Read Before You Die/Dumas, Alexandre/The three musketeers - Alexandre Dumas.epub', file_type: '.epub')}
  
  it 'creates a book' do
    books(:default_book).must_be_instance_of Book
  end

  it 'guesses the author' do
    books(:default_book).best_guess_fields
    books(:default_book).author.must_equal 'Alexandre Dumas'
  end

  it 'guesses the title if it knows the author and has only two fields' do
    books(:default_book).best_guess_fields
    books(:default_book).title.must_equal 'The Three Musketeers'
  end

  it 'gets the previous book' do
    books(:book_with_dumas_as_author).previous.id.must_equal books(:default_book).id
  end

  it 'gets the next book' do 
    books(:default_book).next.id.must_equal books(:book_with_dumas_as_author).id
  end

  it 'should not assign the wrong title' do
    books(:book_with_comma_in_title_and_existing_author).best_guess_fields
    books(:book_with_comma_in_title_and_existing_author).title.must_equal 'This Is It, the Real Thing'
  end

  it "can find authors with commas if they're known" do
    books(:book_with_comma_in_existing_author).best_guess_fields
    books(:book_with_comma_in_existing_author).author.must_equal 'Alexandre Dumas'
  end

  it "doesn't assign the wrong title when assigning a known author" do
    books(:book_with_comma_in_existing_author).best_guess_fields
    books(:book_with_comma_in_existing_author).title.must_equal 'Other Book'
  end

  it 'only replaces hyphens when there are spaces all around' do
    fields = books(:book_with_hyphen_in_title).best_guess_fields
    fields[0].must_include '-'
  end

  it 'replaces _ with : if appropriate' do
    books(:book_with_underscore_as_hyphen).best_guess_fields
    books(:book_with_underscore_as_hyphen).title.must_equal 'Other Book: The Othering'
  end

  it 'should split on _-_' do
    fields = books(:book_with_lots_of_underscores).best_guess_fields
    fields.size.must_equal 2
  end

  it "shouldn't assign the wrong title when the author field and file name don't match" do
    books(:book_with_mismatched_author_and_author).best_guess_fields
    books(:book_with_mismatched_author_and_author).title.must_equal 'Title'

    books(:book_with_mismatched_author_and_author_and_reverse_order).best_guess_fields
    books(:book_with_mismatched_author_and_author_and_reverse_order).title.must_equal 'Title'
  end

  it "should update books in bulk correctly" do
    cache = Book.all_authors
    Book.find_each do |b|
      b.bulk_guess_fields(cache)
    end
    Book.all_authors.must_include "Kelly Link"
    Book.all_authors.must_include "Alexandre Dumas"
    Book.all_authors.size.must_equal 2
  end
end
