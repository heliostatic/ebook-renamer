require 'test_helper'

class BooksControllerTest < ActionController::TestCase
  it "can't get a page without authenticating" do
    get :index
    assert_response :unauthorized
  end 

  describe 'authentication' do
    before { authenticate_user }

    it "can get a page after authenticating" do
      get :index
      assert_response :success
    end
  end
end
