require 'find'

desc 'Import Books'

task :read_in_books => :environment do
  real_book_extensions = %w(.epub .mobi .lit .txt .pdf)
  Find.find("/Volumes/text/Books/").reject { |fn| File.directory?(fn) }.each do |d|
    ft = File.extname(d)
    if real_book_extensions.include? ft
      Book.create(fullpath: d, file_type: ft)
    end
  end
end

task :extract_extension => :environment do
  Book.find_each do |book|
    ext = File.extname(book.fullpath)
    book.file_type = ext.empty? ? 'unknown' : ext
    book.save
  end
end

task :check_authors => :environment do
  cache = Book.all_authors
  Book.find_each do |b|
    b.bulk_guess_fields(cache)
  end
end

task :google_book_search => :environment do
  Book.delay.full_search
end