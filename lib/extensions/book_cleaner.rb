module BookCleaner
  def self.included(base)
    base.extend(ClassMethods)
  end

  def best_guess_fields(expensive=false)
    fields = get_fields_from_path(self.fullpath, self.file_type)
    check_author(fields)
    check_title(fields)
    if expensive
      expensive_check(fields)
    end
    fields
  end

  def bulk_guess_fields(cache, expensive=false)
    @cache = cache
    best_guess_fields(expensive)
  end

  def partial_path
    File.basename(self.fullpath, self.file_type)
  end

  module ClassMethods
    def full_search(number_to_check=500)
      cache = Book.all_authors
      self.no_title.where(for_review: nil).limit(number_to_check).each do |b|
        b.bulk_guess_fields(cache, true)
      end
    end
  end

  private

    def get_fields_from_path(path, ext)
      fields = File.basename(path, ext)
      parent_directory = clean_field(File.dirname(self.fullpath).split('/').last)
      fields_array = split_fields(fields)
      fields_array = add_parent_directory(fields_array, parent_directory)
      fields_array = clean_fields(fields_array)
    end

    def split_fields(fields)
      fields = if fields.count('.') > 3
        fields.split('.')
      else
        fields.split(/(?:\s-\s|_-_)/)
      end
      if fields.length < 2
        fields << File.dirname(self.fullpath).split('/').last
      end
      fields
    end

    def add_parent_directory(array_of_fields, parent_directory)
      unless array_of_fields.include?(parent_directory) || array_of_fields.include?(flip_and_strip(parent_directory))
        array_of_fields << parent_directory if parent_directory.include?(',')
      end
      array_of_fields
    end

    def clean_fields(fields)
      fields = fields.map { |f| clean_field(f) }
      if fields.size > 2
        fields = fields.delete_if { |f| f =~ /.*\d+.*/ }
      end
      fields
    end

    def clean_field(field)
      field.gsub(/_\s/, ': ').gsub('_', ' ').titlecase.strip
    end

    def check_author(fields)
      if self.author.nil? || self.title.nil?
        fields.each_with_index do |f, i|
          author = set_author_if_possible(f)
          if author && author != f
            fields[i] = author
          end
        end
      end
    end

    def flip_and_strip(string)
      string.split(',').reverse.join(' ').strip
    end

    def set_author_if_possible(f)
      if @cache
        author = f if @cache.include?(f)
        author = flip_and_strip(f) if @cache.include?(flip_and_strip(f))
      else
        author = f if Book.where(author: f).where('for_review IS NOT TRUE').any?
        author = flip_and_strip(f) if Book.where(author: flip_and_strip(f)).where('for_review IS NOT TRUE').any?
      end    
      if self.author.nil?
        self.update_attributes(author: author) if author
      end  
      author
    end

    def check_title(fields)
      if self.author && fields.length == 2 && self.title.nil?
        self.update_attributes(title: (fields.first == self.author ? fields.last : fields.first))
      end
    end

    def expensive_check(fields)
      # We'll use the googlebooks gem to check the fields
      logger.info 'Expensive Check Called'
      if self.author.nil? || self.title.nil?
        if fields.size > 2
          fields = fields.delete_if { |f| f =~ /.*\d+.*/ }
        end
        result = GoogleBooks.search(fields.join(' '))
        if result.first
          if self.title.nil?
            self.update_attributes(title: result.first.title)
          end
          if self.author.nil?
            authors = result.first.authors.is_a?(Array) ? result.first.authors.join(', ') : result.first.authors
            self.update_attributes(author: authors)
          end
          self.update_attributes(for_review: true)
        end
      end
    end
end