class BooksController < ApplicationController
  http_basic_authenticate_with :name => "books", :password => "books"
  def show
    @book = Book.find(params[:id])
    @fields = @book.best_guess_fields
  end

  def index
    @book_count = Book.count
    @books_without_authors = Book.no_author.count
    @books_without_titles = Book.no_title.count
    @last_updated_book = Book.last_updated.first
    if params[:status] == 'cleaned'
      @books = Book.cleaned.page(params[:page]).per(5)
    else
      @books = Book.text_search(params[:query]).page(params[:page]).per(5)
    end
  end

  def destroy
    book = Book.find(params[:id])
    next_book = book.next
    book.mark_as_destroyed
    redirect_to root_path
  end

  def update
    @book = Book.find params[:id]

    respond_to do |format|
      if @book.update_attributes(params[:book])
        format.html { redirect_to(@book, :notice => 'Book was successfully updated.') }
        format.json { respond_with_bip(@book) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@book) }
      end
    end
  end

  def authors
    respond_to do |format|
      format.json { render json: Book.all_authors }
    end
  end

  def review
    @books = Book.to_review.order(:updated_at).page(params[:page]).per(50)
    @book_count = Book.to_review.count
    logger.info params
    if params[:layout] == 'bulk'
      render :bulk
    end
  end

  def accept_proposed_changed
    Book.update_all({for_review:false}, {id: params[:book_ids]})
    redirect_to review_books_path layout: 'bulk'
  end

  def runcheck
    Book.delay.full_search(100)
    redirect_to :books, notice: "Books are being searched, my friend"
  end
end
