class Book < ActiveRecord::Base
  include BookCleaner

  attr_accessible :author, :file_type, :fullpath, :title, :dead, :for_review

  validates :file_type, presence: true
  validates :fullpath, presence: true

  default_scope where(dead: false)
  scope :no_author, where('author IS NULL')
  scope :no_title, where('title is NULL')
  scope :cleaned, where('title IS NOT NULL').where('author IS NOT NULL')
  scope :last_updated, order('updated_at DESC').limit(1)
  scope :to_review, where(for_review: true)

  has_paper_trail

  # Should move this out
  def next
    Book.where("books.id > ?", self.id).order("books.id ASC").limit(1).first
  end

  # Should move this out
  def previous
    Book.where("books.id < ?", self.id).order("books.id DESC").limit(1).last
  end

  def mark_as_destroyed
    self.update_attributes(dead: true)
  end

  def self.all_authors
    Book.select('DISTINCT author').where('author IS NOT NULL').where('for_review IS NOT TRUE').order(:author).map(&:author)
  end

  def self.text_search(query)
    if query.present?
      where("title @@ :q or author @@ :q or fullpath @@ :q", q: query).order('id DESC')
    else
      no_title
    end
  end

  private
    
end