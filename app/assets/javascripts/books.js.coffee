# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('.best_in_place').best_in_place()

  filler = (link_type, field_val, id) ->
    field_selector = $("table."+id+" span[data-attribute=" + link_type + "]")
    field_selector.trigger "click"
    input_selector = $("table."+id+" input[name=" + link_type + "]")
    input_selector.val field_val
  
  $(".field_link").on "click", ->
    link_type = $(@).text()
    field_val = $(@).siblings(".field").text()
    id = $(@).parents('ul').attr('class')
    filler(link_type, field_val, id)

  $('.name_toggle').on "click", ->
    # TODO: Iterate over array of books, flip by class of TR instead of assuming one book
    fullpath = $(@).parents('table').children('tbody').children('tr').children('td.path').data('fullpath')
    filename = $(@).parents('table').children('tbody').children('tr').children('td.path').text()
    $(@).parents('table').children('tbody').children('tr').children('td.path').data('fullpath', filename)
    $(@).parents('table').children('tbody').children('tr').children('td.path').text(fullpath)

  $('.best_in_place').bind "ajax:success", ->
    klass = $(@).data('attribute')
    id = $(@).data('url').match(/\d+/)[0]
    $("h3#"+id+" ."+klass+"").text($(@).text())

  $('.flipper_link').on 'click', ->
    link_type = $(@).attr('id')
    field_val = $(@).siblings(".field").text().split(',').reverse().join(' ').trim()
    id = $(@).parents('ul').attr('class')
    filler(link_type, field_val, id)

  $('ul.typeahead').on 'mousedown', (e) ->
    e.preventDefault()

  # use this hash to cache search results
  window.query_cache = {}
  $(document).on "keydown", ".best_in_place[data-attribute='author'] input", ->
    $(@).typeahead source: (query, process) ->
      # if in cache use cached value, if don't wanto use cache remove this if statement
      if query_cache[query]
        process query_cache[query]
        return
      unless typeof searching is "undefined"
        clearTimeout searching
        process []
      searching = setTimeout(->
        $.getJSON "/books/authors",
          q: query
        , (data) ->
          
          # save result to cache, remove next line if you don't want to use cache
          query_cache[query] = data
          
          # only search if stop typing for 300ms aka fast typers
          process data

      , 300) # 300 ms
