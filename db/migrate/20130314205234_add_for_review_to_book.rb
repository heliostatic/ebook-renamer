class AddForReviewToBook < ActiveRecord::Migration
  def change
    add_column :books, :for_review, :boolean
  end
end
