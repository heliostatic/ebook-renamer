class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.text :fullpath
      t.string :file_type
      t.string :title
      t.string :author

      t.timestamps
    end
  end
end
