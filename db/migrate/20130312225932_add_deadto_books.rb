class AddDeadtoBooks < ActiveRecord::Migration
  def change
    add_column :books, :dead, :boolean, default: false
  end
end
