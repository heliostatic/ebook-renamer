EbookRenamer::Application.routes.draw do
  root :to => "books#index"
  resources :books do 
    collection do 
      get 'page/:page', :action => :index
      get 'authors'
      get 'review'
      get 'runcheck'
      put :accept_proposed_changed
    end
  end
end